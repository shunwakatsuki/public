公開したいものを`downloads`に置くためだけのリポジトリ．
他の(非公開)リポジトリからPipelines使ってuploadしてたりする．

# リンク
[Downloads](https://bitbucket.org/shunwakatsuki/public/downloads/)

# やり方メモ
基本的には公式のこれ
[Deploy build artifacts to Bitbucket Downloads - Atlassian Documentation](https://confluence.atlassian.com/bitbucket/deploy-build-artifacts-to-bitbucket-downloads-872124574.html)
を参考にしてやれば良い．
ただし，private repositoryだと
[Solved: Deploy build artifacts to Bitbucket Downloads - no...](https://community.atlassian.com/t5/Bitbucket-questions/Deploy-build-artifacts-to-Bitbucket-Downloads-not-working/qaq-p/754830)
にあるように認証情報を入れる必要がある？
- `-u username:password`
- パスワードはbitbucketの環境変数を使うと良い？
